using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    // Start is called before the first frame update

    CharacterController cc;
    [SerializeField] Animator anim;
    [SerializeField] private float horizontalSpeed;
    [SerializeField] private float verticalSpeed;
    [SerializeField] private float speed;
    [SerializeField] private float xrotation;
    [SerializeField] float hAxis;
    [SerializeField] float vAxis;




    [SerializeField] GameObject model;
    [SerializeField] Camera cam;

    
    void Awake(){
        cc = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();

        
    }

    void Start(){
        xrotation =0;
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 movementInput = Vector3.zero;
        
        if(Input.GetKey(KeyCode.W)){
            movementInput.z = 1;   
        }
        if(Input.GetKey(KeyCode.S)){
            movementInput.z = -1;
        }
        if(Input.GetKey(KeyCode.D)){
            movementInput.x = 1;
        }
        if(Input.GetKey(KeyCode.A)){
            movementInput.x = -1;   
        }
        Move(movementInput);

        
        hAxis = horizontalSpeed * Input.GetAxis("Mouse X") * Time.deltaTime;
        vAxis = verticalSpeed * Input.GetAxis("Mouse Y") * Time.deltaTime;
        xrotation -= vAxis;
        xrotation = Mathf.Clamp(xrotation,-90f,90f);
        cam.transform.localRotation = Quaternion.Euler(xrotation,0f,0f);
        transform.Rotate(0,hAxis,0);

        
    }

    void LateUpdate(){
        
    }


    void Move(Vector3 direcition){
        direcition = transform.TransformDirection(direcition);
        cc.SimpleMove(direcition.normalized * speed);
        //Debug.Log(direcition);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    //Variables
    RaycastHit hit;
    Vector3 newPosition;

    [SerializeField] private float mouseSensitivy;
    private Transform parent;
    [SerializeField] private GameObject target;
    
    [SerializeField]private Vector3 positionOrig;
    public float hitDistance;

    //bool camerabool;


    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent;
        //camerabool = false;

    }
    
    void Awake()
    {
        //positionOrig =  gameObject.transform.position;

    }

    // Update is called once per frame
    void Update()
    {

        Rotate();
        


        

    }

    private void LateUpdate()
    {
       
    }

    private void Rotate(){
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivy * Time.deltaTime;
        
       
        parent.Rotate(Vector3.up,mouseX);

    }
}

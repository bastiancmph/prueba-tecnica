using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectGun : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject shot;
    public GameObject shot2;
    public GameObject shot3;
    public GameObject gun1;
    public GameObject gun2;
    public GameObject gun3;

    public int gun;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter(Collider collision)
    {
        if(collision.transform.tag == "Player"){
            if(gun == 1){
                gun2.SetActive(false);
                gun3.SetActive(false);
                gun1.SetActive(true);
                shot2.SetActive(false);
                shot3.SetActive(false);
                shot.SetActive(true);

            }else if(gun==2){
                gun3.SetActive(false);
                gun1.SetActive(false);
                gun2.SetActive(true);
                shot3.SetActive(false);
                shot.SetActive(false);
                shot2.SetActive(true);

            }else{
                gun1.SetActive(false);
                gun2.SetActive(false);
                gun3.SetActive(true);
                shot.SetActive(false);
                shot2.SetActive(false);
                shot3.SetActive(true);
            }
        }

    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitalShot : MonoBehaviour
{
    public gunScriptableObject gun;
    private GameObject bullet;
    private int numberGun;
    private float force;
    [SerializeField] private Camera camera;   
    private GameObject BalaTemporal;
    // Start is called before the first frame update
    // Start is called before the first frame update
    void Start()
    {
         bullet = gun.prefabBullet;
         force = gun.force;
         numberGun = gun.gunNumber;

    }

    // Update is called once per frame
    void Update()
    {
         if(Input.GetMouseButtonDown(0)){
            Shoot();
        }

        
        
    }


    void Shoot(){
        RaycastHit hit;
        if(BalaTemporal == null){
           
            if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hit)){
                //Debug.Log(hit.transform.name);
                Debug.DrawLine(camera.transform.position, hit.point,Color.red);
                BalaTemporal = Instantiate(bullet, camera.transform.position, camera.transform.rotation);
                Rigidbody rb = BalaTemporal.GetComponent<Rigidbody>();
                Vector3 dir = hit.point - camera.transform.position ;
                //dir.Normalize();
                //Agregar la fuerza a la Bala
                rb.AddForce(dir * force);
                
            }
            
             //Destroy(BalaTemporal, 3f);
        }
        if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hit)){
                //Debug.Log(hit.transform.name);
           
                Debug.DrawLine(camera.transform.position, hit.point,Color.red);
        }

        //BalaTemporal = null;
    }

    

    
}

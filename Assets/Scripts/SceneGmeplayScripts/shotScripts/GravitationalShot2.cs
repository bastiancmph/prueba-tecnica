using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitationalShot2 : MonoBehaviour
{
    public gunScriptableObject gun;
    private GameObject bullet;
    private int numberGun;
    private float force;
    public bool isHoldingObject;
    public GameObject holdingObject;
    [SerializeField] private Camera camera;   
    public GameObject aimObject;
    // Start is called before the first frame update
    // Start is called before the first frame update
    void Start()
    {
         bullet = gun.prefabBullet;
         force = gun.force;
         numberGun = gun.gunNumber;
         isHoldingObject = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            //Shoot();
            if(aimObject == null){
                if(isAimingGravitiObject()){
                    float distance = Vector3.Distance(aimObject.transform.position,transform.position);
                    Debug.Log(aimObject);
                    Vector3 dir = camera.transform.position -  aimObject.transform.position;
                    Rigidbody rb = aimObject.GetComponent<Rigidbody>();
                    rb.AddForce(dir * 2);

                }
            }else{
                Lock();
               

            }

        }
        if(Input.GetMouseButtonDown(1)){
             if(isHoldingObject){
                isHoldingObject = false;
                holdingObject.transform.parent = null;
                Rigidbody rb = holdingObject.GetComponent<Rigidbody>();
                rb.useGravity = true;
                rb.constraints = RigidbodyConstraints.None;
                rb.velocity = (camera.transform.forward) * force;
                
                holdingObject = null;            
        }

            




        }




        
        
    }

    public void Lock(){

        if(aimObject != null){
            isHoldingObject = true;
            holdingObject = aimObject;
            aimObject = null;

            holdingObject.transform.position = camera.transform.position + camera.transform.forward * 3f;
            holdingObject.transform.rotation = transform.rotation;

            Rigidbody rb = holdingObject.GetComponent<Rigidbody>();
            rb.useGravity = false;
            rb.constraints = RigidbodyConstraints.FreezeAll;
            rb.velocity = Vector3.zero;
            holdingObject.transform.parent = camera.transform;
        }


    }


    void Shoot(){
        /* RaycastHit hit;
        if(BalaTemporal == null){
           
            if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hit)){
                //Debug.Log(hit.transform.name);
                Debug.DrawLine(camera.transform.position, hit.point,Color.red);
                BalaTemporal = Instantiate(bullet, camera.transform.position, camera.transform.rotation);
                Rigidbody rb = BalaTemporal.GetComponent<Rigidbody>();
                Vector3 dir = hit.point - camera.transform.position ;
                //dir.Normalize();
                //Agregar la fuerza a la Bala
                rb.AddForce(dir * force);
                
            }
            
             //Destroy(BalaTemporal, 3f);
        }
        if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hit)){
                //Debug.Log(hit.transform.name);
           
                Debug.DrawLine(camera.transform.position, hit.point,Color.red);
        } */

        //BalaTemporal = null;
    }




    private bool isAimingGravitiObject(){
        RaycastHit hit;
        bool isAimingObject = Physics.Raycast(camera.transform.position, camera.transform.forward, out hit);
        if(isAimingObject &&  hit.transform.tag == "gravitiEnviroment"){
            this.aimObject = hit.transform.gameObject;

        }
        return isAimingObject;
    }

    

    
}

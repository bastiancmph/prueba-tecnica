using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerAnimatorScript : MonoBehaviour
{
    private Animator animator;

    // Numero de animaciones disponibles
    [SerializeField] private int numAnimations;
    [SerializeField] private int numAnimation;
    //[SerializeField] private MenuToGameplay menuToGamePlay;


    // Start is called before the first frame update
    void Start()
    {
        numAnimation = Settings.instance.animationSelected;
        numAnimations = Settings.instance.numAnimations;
        Debug.Log(Settings.instance.animationSelected);
        animator = this.GetComponent<Animator>();
        animator.SetFloat("numAnimation",numAnimation);

        //Debug.Log(animator);  
    }

    public void setAnimationAnimator(int value){
        
       if(value < numAnimations){
            animator.SetFloat("numAnimation",value);
            Settings.instance.animationSelected = value;
       }else{
           Debug.Log(value + " no es un numero de animacion valido");
       }
             
            
        
    }

}

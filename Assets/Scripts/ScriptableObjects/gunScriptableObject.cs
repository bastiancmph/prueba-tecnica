using UnityEngine;

[CreateAssetMenu(fileName = "new Gun", menuName = "Gun")]
public class gunScriptableObject : ScriptableObject

//ScriptableObject for gun

{
    public GameObject prefabBullet;
    public int gunNumber;
    public float force;
}
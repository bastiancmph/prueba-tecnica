using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{

    
    public Button nextButton;
    public string scene;
    // Start is called before the first frame update
    void Start()
    {
        nextButton.onClick.AddListener(TaskOnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void TaskOnClick(){
         SceneManager.LoadScene(scene);
    }

}

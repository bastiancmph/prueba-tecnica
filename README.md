# README #

Repositorio para uso exclusivo en prueba tecnica.
### Controles ###

La seleccion de armas consiste en tocar el arma que desea usar.
Armas disparan con click izquierdo.
GravitiGun hace pull con click izquierdo y push con click derecho.


pistola: tiro parabolico (primer requerimiento).
rifle:  tiro gravitacional(segundo requerimiento).
GravitiGun: tiro de pull and push(tercer requerimiento).


### Requerimientos  ###
al iniciar, se desplegará una interfaz en canvas que nos mostrará el modelo, esta incluirá 4 botones, 3 de ellos al ser clickeados el personaje cambiara su animacion por la asignada a cada botón, mientras el ultimo
determinara la aniamcion a seleccionar, al terminar este proceso se enviará al usuario a una segunda escena. ✔️

en esta segunda fase la vista será en primera persona, se podra ver al personaje reproduciendo la animacion asignada en una pequeña esquina en el ui.
por otro lado el usuario podra escoger entre 3 armas disintas en el suelo, cada una de ellas disparará de una forma distinta. ✔️

la primera realizará un disparo con trayectoria parabolica.
la segunda generará un campo alrdedor donde atraerá objetos cercanos haciendolos orbitar alrededor del proyectil.
la tercera será libre pero deberá incluir alguna propiedad fisica similar a las anteriores descritas.✔️

todas las armas tendran valores personalizables por medio de un scriptable object, que debera ser cargado al iniciar el juego.✔️